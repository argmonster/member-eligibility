create table if not exists member_eligibility_batches (
    BATCH_ID integer primary key,
    BATCH_DATE text,
	 SOURCE_FILE text
);
  
create table if not exists member_eligibility_batch_data(
    BATCH_ID integer,
    MEMBER_ID text(10) not null,
    FIRST_NAME text(50),
    LAST_NAME text(50),
    ELIGIBILITY_START text(10) not null,
    ELIGIBILITY_END text(10),
	 foreign key(BATCH_ID) references member_eligibility_batches(BATCH_ID)
);

 create index if not exists idx_batch_members on member_eligibility_batch_data ( MEMBER_ID );
  
 create table if not exists member_eligibility_errors (
    BATCH_ID integer,
    MEMBER_ID text(10) not null,
    ERROR_MESSAGE text not null,
	 foreign key(BATCH_ID) references member_eligibility_batches(BATCH_ID)
 );
  
 create table if not exists member_eligibility (
    MEMBER_ID text(10) not null,
    FIRST_NAME text(50),
    LAST_NAME text(50),
    ELIGIBILITY_START text(10) not null,
    ELIGIBILITY_END text(10)
 );

 create unique index if not exists idx_members on member_eligibility ( MEMBER_ID );



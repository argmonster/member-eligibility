# summary

This project migrates pipe delimited text into database tables

# need

Customer XYZ periodically delivers pipe-delimited files to our sftp server.

# design 

This project will follow a workflow of:

1. install the project
2. run the project
3. optionally clean/uninstall the project

Running the project will:

1. read the file specified at runtime into a sql database

# quick start

```
$ git clone https://gitlab.com/argmonster/member-eligibility.git
$ cd member-eligibility
$ ./INSTALL
```

This will install an executable script	`process-eligibility-data` which can be used to parse files found in the shared volume `/etc/member-eligibility/source`. The script is for ease of use and simply calls the underlying docker command `sudo docker run --rm -v member-data:/etc/member-eligibility member-elig $1`

```
$ ./process-member-eligibility member_eligibility.20200101.psv
```

`process-member-eligibility` can be run on as often as needed

To remove the persistant volume

```
$ ./CLEAN
```

# implementation

- Docker will be utilized to install and run the project inside a re-deployable container
  - The docker container will install sqlite 3.34.1 and python 3.8.6
- Installation will be managed through a shell script to call the needed docker commands in the correct order
- Installation will provision a persistant volume to store the source and target data
- Installation will execute the SQL scripts within the container needed to intialize/setup the target database and tables
  -  The container will initialize a sqlite database
- Installation will build the parser program
  - The parser code will reside in / be executable from the docker container 
  - The parser executable will accept the name of the file to be parsed as an argument
  - The parser will load the named file from a folder in the docker filesystem, parse the file, and persist its contents into the sqlite database.

# datamodel/business rules

- `MEMBER_ID`
  - Non-null
  - A 10-character alphanumeric string, unique per member
- `FIRST_NAME`, `LAST_NAME` 
  - Non-null
  - Alphanumeric string, maximum 50 characters per field
  - By convention, first name and last name will be stored using all upper-case letters
- `ELIGIBILITY_START`
  - Non-null
  - Formatted `YYYY-MM-DD`
  - Indicates when the member became eligible for our service.  
  - `ELIGIBILITY_START` will always be less-than or equal to the file creation date.
  - `ELIGIBILITY_START` will never change for a given member
- `ELIGIBILITY_END`
  - Nullable; Null values are transmitted as empty columns in the member eligibility file.
  - If populated, we expect that this date will be greater than the most recent `ELIGIBILITY_START` date for a given member.  
  - `ELIGIBILITY_END` will always be less-than or equal to the file creation date.
  - After a member's `ELIGIBILITY_END` date is set in a `member_eligibility` file, it will never change 

# to do

- code review
- integration tests that validate the behavior of your parser program.
- business rule enforsement 
- detect new data in the database, and generate an extract of member eligibility data.

# assumptions

- Assume that the specified file is present, it is correctly formatted, and it follows the business rules described above.

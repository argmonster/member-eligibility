FROM python:3.8.6 as setup
WORKDIR ./
COPY ./db/setup.sql ./db/
COPY ./data/* ./data/
RUN dpkg --add-architecture i386 && apt update
RUN apt install -y wget libc6:i386
RUN wget https://sqlite.org/2021/sqlite-tools-linux-x86-3340100.zip 
RUN unzip -jd /usr/local/bin \
	./sqlite-tools-linux-x86-3340100.zip \
	sqlite-tools-linux-x86-3340100/sqlite3 && \
	rm ./sqlite-tools-linux-x86-3340100.zip
VOLUME /etc/member-eligibility

FROM setup as app
WORKDIR ./
COPY ./app/process_file.py .
VOLUME /etc/member-eligibility
ENTRYPOINT ["python3", "./process_file.py"]
